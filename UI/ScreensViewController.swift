//
//  ScreensViewController.swift
//  Meeting
//
//  Created by Lucian RADUTI on 06/01/15.
//  Copyright (c) 2015 QUALTEH. All rights reserved.
//

import UIKit

class ScreensViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ScreensDoneDelegate {

    var tableViewController = UITableViewController(style: UITableViewStyle.Plain)
    var activityIndicator: UIActivityIndicatorView! = nil
    var refreshControl = UIRefreshControl()
    var activeIndex: Int = -1
    var isLoaded: Bool = false
    
    let cellScreensTableIdentifier = "CellScreensTableIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableViewController.tableView.registerClass(UITableViewCell.self,  forCellReuseIdentifier: cellScreensTableIdentifier)
        tableViewController.tableView.dataSource = self
        tableViewController.tableView.delegate = self
        
        tableViewController.refreshControl = self.refreshControl
        self.refreshControl.addTarget(self, action: "refreshScreensTable", forControlEvents: UIControlEvents.ValueChanged)
        self.view.addSubview(tableViewController.tableView)
        
        activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 100, 100)) as UIActivityIndicatorView
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        self.view.addSubview(activityIndicator)
        
        Global.getScreensFromDatabase(self)
        
        if ( (Global.dataManager.settings.serverURL != "") && (Global.isUserValid() == true) ) {
            Global.getScreensFromServer(self)
        }
        
        isLoaded = true
    }
    
    override func viewDidAppear(animated: Bool) {
        // check if we have server url
        if (Global.dataManager.settings.serverURL == "") {
            self.tabBarController?.selectedIndex = TabBarIndex.Settings.rawValue
        } else {
            // check if we have valid user
            if (Global.isUserValid() == false) {
                self.tabBarController?.selectedIndex = TabBarIndex.Login.rawValue
            } else {
                if (isLoaded == true) {
                    isLoaded = false
                } else {
                    Global.getScreensFromDatabase(self)
                }
            }
        }
    }
    
    private func GoToLogin() {
        self.tabBarController?.selectedIndex = TabBarIndex.Login.rawValue
    }
    
    func GetScreensDone(success: Bool, statusCode: Int, message: String) {
        self.refreshControl.endRefreshing()
        
        if (statusCode == 200) {
            if (success == true) {
                tableViewController.tableView.reloadData()
            } else {
                var errorLoadScreensAlert = UIAlertController(title: "IQboard", message: "The screens list could not be loaded. " + message, preferredStyle: UIAlertControllerStyle.Alert)
                errorLoadScreensAlert.addAction(UIAlertAction(title: "Close", style: .Default, handler: nil))
                presentViewController(errorLoadScreensAlert, animated: true, completion: nil)
            }
        } else {
            if (statusCode == 403) {
                var errorUserAlert = UIAlertController(title: "IQboard", message: "This user is not active!", preferredStyle: UIAlertControllerStyle.Alert)
                errorUserAlert.addAction(UIAlertAction(title: "Go to Login", style: .Default, handler: { (action: UIAlertAction!) in self.GoToLogin() } ))
                presentViewController(errorUserAlert, animated: true, completion: nil)
            } else {
                var errorLoadScreensAlert = UIAlertController(title: "IQboard", message: "The screens list could not be loaded. Check your data connection and try again.", preferredStyle: UIAlertControllerStyle.Alert)
                errorLoadScreensAlert.addAction(UIAlertAction(title: "Close", style: .Default, handler: nil))
                presentViewController(errorLoadScreensAlert, animated: true, completion: nil)
            }

        }
    }
    
    func ActivateScreenDone(success: Bool, statusCode: Int, message: String) {
        activityIndicator.stopAnimating()
        
        if (statusCode == 200) {
            if (success == true) {
                if (activeIndex >= 0) {
                    Global.dataManager.setCurrentScreen(Global.dataManager.screens[activeIndex])
                }
                self.tabBarController?.selectedIndex = TabBarIndex.Touchpad.rawValue
            } else {
                var errorSelectedFileAlert = UIAlertController(title: "IQboard", message: "The screen could not be activated. " + message, preferredStyle: UIAlertControllerStyle.Alert)
                errorSelectedFileAlert.addAction(UIAlertAction(title: "Close", style: .Default, handler: nil))
                presentViewController(errorSelectedFileAlert, animated: true, completion: nil)
            }
        } else {
            if (statusCode == 403) {
                var errorUserAlert = UIAlertController(title: "IQboard", message: "This user is not active!", preferredStyle: UIAlertControllerStyle.Alert)
                errorUserAlert.addAction(UIAlertAction(title: "Go to Login", style: .Default, handler: { (action: UIAlertAction!) in self.GoToLogin() } ))
                presentViewController(errorUserAlert, animated: true, completion: nil)
            } else {
                var errorSelectedFileAlert = UIAlertController(title: "IQboard", message: "The screen could not be activated. Check your data connection and try again.", preferredStyle: UIAlertControllerStyle.Alert)
                errorSelectedFileAlert.addAction(UIAlertAction(title: "Close", style: .Default, handler: nil))
                presentViewController(errorSelectedFileAlert, animated: true, completion: nil)
            }
        }
    }
    
    func refreshScreensTable() {
        Global.getScreensFromServer(self)
    }

    func tableView(tableView: UITableView,
        numberOfRowsInSection section: Int) -> Int {
        return Global.dataManager.screens.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
        var cell = tableView.dequeueReusableCellWithIdentifier(cellScreensTableIdentifier) as? UITableViewCell
        
        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellScreensTableIdentifier)
        }
    
        cell!.textLabel?.text = Global.dataManager.screens[indexPath.row].name

        cell!.imageView?.image = UIImage(named: "screen")
    
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var selectedScreenAlert = UIAlertController(title: "IQboard", message: "Do you want to make active <" + Global.dataManager.screens[indexPath.row].name + "> screen on the meeting screen?", preferredStyle: UIAlertControllerStyle.Alert)
        selectedScreenAlert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { (action: UIAlertAction!) in self.selectScreen(indexPath.row) } ))
        selectedScreenAlert.addAction(UIAlertAction(title: "No", style: .Default, handler: nil))
        presentViewController(selectedScreenAlert, animated: true, completion: nil)
    }
    
    private func GoToMeetings() {
        self.tabBarController?.selectedIndex = TabBarIndex.Meetings.rawValue
    }
    
    func selectScreen(index: Int) {
        if (Global.dataManager.getCurrentMeeting() != nil) {
            activityIndicator.startAnimating()
            activeIndex = index
            Global.activateScreen(self, idMeeting:Global.dataManager.getCurrentMeeting().id, idScreen: Global.dataManager.screens[index].id)
        } else {
            var errorUserAlert = UIAlertController(title: "IQboard", message: "You have no current meeting!", preferredStyle: UIAlertControllerStyle.Alert)
            errorUserAlert.addAction(UIAlertAction(title: "Go to Meetings", style: .Default, handler: { (action: UIAlertAction!) in self.GoToMeetings() } ))
            presentViewController(errorUserAlert, animated: true, completion: nil)
        }
    }
}

