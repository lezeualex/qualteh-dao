//
//  ScreensTable.swift
//  Meeting
//
//  Created by Lucian RADUTI on 18/01/15.
//  Copyright (c) 2015 QUALTEH. All rights reserved.
//

import Foundation

class ScreensTable {
    let table: String = "Screens"
    let id: String = "ID"
    let name: String = "Name"
    
    func createTable(database: COpaquePointer) -> Bool {
        let query = "CREATE TABLE IF NOT EXISTS \(table) (\(id) TEXT, \(name) TEXT);"
        var errMsg:UnsafeMutablePointer<Int8> = nil
        let result = sqlite3_exec(database, query, nil, nil, &errMsg)
        if (result == SQLITE_OK) {
            return true;
        }
        
        return false;
    }
}