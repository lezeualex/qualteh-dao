//
//  Screen.swift
//  Meeting
//
//  Created by Lucian RADUTI on 12/01/15.
//  Copyright (c) 2015 QUALTEH. All rights reserved.
//

import Foundation

class Screen: NSObject, NSCoding {
    var id: String!
    var name: String!
    
    init(id: String!, name: String!) {
        self.id = id
        self.name = name
    }
    
    init(screen: Screen) {
        self.id = screen.id
        self.name = screen.name
    }
    
    required convenience init(coder decoder: NSCoder) {
        self.init()
        self.id = decoder.decodeObjectForKey("id") as String?
        self.name = decoder.decodeObjectForKey("name") as String?
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.id, forKey: "id")
        coder.encodeObject(self.name, forKey: "name")
    }
    
    override init()
    {
        id = ""
        name = ""
    }
}