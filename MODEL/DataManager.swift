//
//  UserDataManager.swift
//  Meeting
//
//  Created by Lucian RADUTI on 12/01/15.
//  Copyright (c) 2015 QUALTEH. All rights reserved.
//

import Foundation

protocol SettingsDoneDelegate {
    func TestServerConnectionDone(success: Bool, statusCode: Int, message: String)
}

protocol UserDoneDelegate {
    func LoginDone(success: Bool, statusCode: Int, message: String)
}

protocol UserTokenDoneDelegate {
    func CheckUserToken(success: Bool, statusCode: Int, message: String)
}

protocol MeetingsDoneDelegate {
    func GetMeetingsDone(success: Bool, statusCode: Int, message: String)
}

protocol ScreensDoneDelegate {
    func GetScreensDone(success: Bool, statusCode: Int, message: String)
    func ActivateScreenDone(success: Bool, statusCode: Int, message: String)
}

protocol FilesDoneDelegate {
    func GetFilesDone(success: Bool, statusCode: Int, message: String)
    func ActivateFileDone(success: Bool, statusCode: Int, message: String)
}

class DataManager: NSObject {
    // DATA
    var database:COpaquePointer = nil
    var settings: Settings! = nil
    var user: User! = nil
    var meetings: [Meeting]! = nil
    var screens: [Screen]! = nil
    var files: [File]! = nil
    var annotations: [Annotation]! = nil
    var point: TouchPoint = TouchPoint()
    
    private var currentMeeting: Meeting! = nil
    private var currentScreen: Screen! = nil
    private var currentFile: File! = nil
    
    // DELEGATES
    var settingsDoneDelegate: SettingsDoneDelegate! = nil
    var userDoneDelegate: UserDoneDelegate! = nil
    var checkUserTokenDoneDelegate: UserTokenDoneDelegate! = nil
    var meetingsDoneDelegate: MeetingsDoneDelegate! = nil
    var screensDoneDelegate: ScreensDoneDelegate! = nil
    var filesDoneDelegate: FilesDoneDelegate! = nil
    
    // INIT
    override init () {
        super.init()
    }

    deinit {
        deinitData()
    }
    
    // SET
    func setCurrentMeeting(newCurrentMeeting: Meeting!, save: Bool = true) {
        if (save == true) {
            if (newCurrentMeeting != nil)
            {
                currentMeeting = newCurrentMeeting
                CurrentObjectDao(database: database).save(currentMeeting)
            } else {
                CurrentObjectDao(database: database).delete(ObjectType.meeting.rawValue)
            }
        } else {
            currentMeeting = newCurrentMeeting
        }
    }
    
    func setCurrentScreen(newCurrentScreen: Screen!, save: Bool = true) {
        if (save == true) {
            if (newCurrentScreen != nil) {
                currentScreen = newCurrentScreen
                currentFile = nil
                CurrentObjectDao(database: database).save(currentScreen)
            } else {
                CurrentObjectDao(database: database).delete(ObjectType.screen.rawValue)
            }
        } else {
            currentScreen = newCurrentScreen
            if (currentScreen != nil) {
                currentFile = nil
            }
        }
    }
    
    func setCurrentFile(newCurrentFile: File!, save: Bool = true) {
        if (save == true) {
            if (newCurrentFile != nil) {
                currentFile = newCurrentFile
                currentScreen = nil
                CurrentObjectDao(database: database).save(currentFile)
            } else {
                CurrentObjectDao(database: database).delete(ObjectType.file.rawValue)
            }
        } else {
            currentFile = newCurrentFile
            if (currentFile != nil) {
                currentScreen = nil
            }
        }
    }
    
    // GET
    func getCurrentMeeting() -> Meeting! {
        return currentMeeting;
    }
    
    func getCurrentScreen() -> Screen! {
        return currentScreen
    }
    
    func getCurrentFile() -> File! {
        return currentFile
    }
    
    // DATABASE
    private func openDatabase() -> Bool {
        let result = sqlite3_open(getDatabaseFilePath(), &database)
        
        if result == SQLITE_OK {
            createDatabaseTables()
            return true
        }
        
        return false
    }
    
    private func closeDatabase() {
        sqlite3_close(database)
        database = nil
    }
    
    func initData() {
        settings = Settings()
        user = User()
        meetings = [Meeting]()
        screens = [Screen]()
        files = [File]()
        annotations = [Annotation]()
        currentMeeting = nil
        currentScreen = nil
        currentFile = nil
        
        openDatabase()
        
        getSettingsFromDatabase()
        getUserFomDatabase()
        
        if ( (settings.serverURL != "") && (user.isValid() == true) ) {
            getMeetingsFromDatabase(nil, setCurrent: false)
            getScreensFromDatabase(nil, setCurrent: false)
            getFilesFromDatabase(nil, setCurrent: false)
            
            CurrentObjectDao(database: database).getAll()
            
            getMeetingsFromServer(nil)
            getScreensFromServer(nil)
            getScreensFromServer(nil)
        }
        
    }
    
    func deinitData() {
        settings = nil
        user = nil
        if (meetings != nil) {
            meetings.removeAll(keepCapacity: false)
            meetings = nil
        }
        if (screens != nil) {
            screens.removeAll(keepCapacity: false)
            screens = nil
        }
        if (files != nil) {
            files.removeAll(keepCapacity: false)
            files = nil
        }
        if (annotations != nil) {
            annotations.removeAll(keepCapacity: false)
            annotations = nil
        }
        currentMeeting = nil
        currentScreen = nil
        currentFile = nil
        
        closeDatabase()
        
    }
    
    private func getDatabaseFilePath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentsDirectory = paths[0] as NSString
        
        return documentsDirectory.stringByAppendingPathComponent(Global.databaseName) as String
    }
    
    private func createDatabaseTables() {
        if database != nil {
            var settingsTable: SettingsTable = SettingsTable()
            let createSettingsTableOK = settingsTable.createTable(database)
            
            var userTable: UserTable = UserTable()
            let createUserTableOK = userTable.createTable(database)
            
            var meetingsTable: MeetingsTable = MeetingsTable()
            let createMeetingsTableOK = meetingsTable.createTable(database)
            
            var screensTable: ScreensTable = ScreensTable()
            let createScreensTableOK = screensTable.createTable(database)
            
            var filesTable: FilesTable = FilesTable()
            let createFilesTableOK = filesTable.createTable(database)
            
            var currentObjectsTable: CurrentObjectsTable = CurrentObjectsTable()
            let createCurrentObjectsOK = currentObjectsTable.createTable(database)
        }
    }
    
    // TEST CONNECTION
    func testServerConnection(controller: SettingsViewController) {
        settingsDoneDelegate = controller
        Server.testConnection(testServerConnectionDone)
    }
    
    private func testServerConnectionDone(error: NSError?, data: NSData?, statusCode: Int) {
        var message: String = ""
        var success: Bool = false
        
        // if we get response from the server will check the response
        if ( (statusCode == 200) && (error == nil) && (data != nil) ) {
            let json = JSON(data: data!)
            
            message = json["message"].stringValue
            // if success = true we have an answer from server => connection OK
            if (json["success"].boolValue == true) {
                success = true
            }
            
            if (settingsDoneDelegate != nil) {
                settingsDoneDelegate.TestServerConnectionDone(success, statusCode: statusCode, message: message)
            }
        }
    }
    
    // SETTINGS
    private func getSettingsFromDatabase() {
        SettingsDao(database: database).get()
    }
    
    func saveSettingsToDatabase() {
        SettingsDao(database: database).save()
    }
    
    // USER
    func loginUser(controller: LoginViewController, userName: String, userPassword: String) {
        userDoneDelegate = controller
        
        // save the user name in global variables zone
        user.userName = userName
        user.token = ""
        
        // delete user from database
        UserDao(database: database).delete()
        
        let userString = "{\"type\":2,\"username\":\"\(userName)\",\"password\":\"\(userPassword)\"}"
        if let userData = (userString as NSString).dataUsingEncoding(NSUTF8StringEncoding) {
            Server.login(userData, done: loginUserDone)
        }
    }
    
    private func loginUserDone(error: NSError?, data: NSData?, statusCode: Int) {
        let text = NSString(data: data!, encoding: NSUTF8StringEncoding)
        var message: String = ""
        var success: Bool = false
        
        // if we get response from the server will check the response
        if ( (statusCode == 200) && (error == nil) && (data != nil) ) {
            // connection OK, check server response
            let json = JSON(data: data!)

            message = json["message"].stringValue
            
            // if success = true we will save the token received from the server
            if (json["success"].boolValue == true) {
                success = true
                user.token = json["data"]["token"].stringValue
                
                // save user to database
                let saveOK = UserDao(database: database).save()
            } else {
                user.token = ""
            }
        }
        
        if (userDoneDelegate != nil) {
            userDoneDelegate.LoginDone(success, statusCode: statusCode, message: message)
        }
    }
    
    func checkUserToken(controller: TouchpadViewController) {
        checkUserTokenDoneDelegate = controller
        Server.checkUserToken(checkUserTokenDone)
    }
    
    private func checkUserTokenDone(error: NSError?, data: NSData?, statusCode: Int) {
        var message: String = ""
        var success: Bool = false
        
        // if we get response from the server will check the response
        if ( (statusCode == 200) && (error == nil) && (data != nil) ) {
            let json = JSON(data: data!)
        
            // if success = false we do not have any more a valid token
            if (json["success"].boolValue == false) {
                user.token = ""
                UserDao(database: database).save()
            } else {
                success = true
            }
        }
        
        checkUserTokenDoneDelegate.CheckUserToken(success, statusCode: statusCode, message: message)
    }
    
    private func getUserFomDatabase() {
        UserDao(database: database).get()
    }
    
    func isUserValid() -> Bool {
        return user.isValid()
    }
    
    // TOUCH COORDINATES
    func sendTouchCoordinates(xCoordinate: Float, yCoordinate: Float) {
        let touchCoordinateString = "{\"x\": \(xCoordinate), \"y\": \(yCoordinate)}"
        if let touchCoordinatesData = (touchCoordinateString as NSString).dataUsingEncoding(NSUTF8StringEncoding) {
            Server.sendTouchCoordinates(touchCoordinatesData, done: sendTouchCoordinatesDone)
        }
    }
    
    private func sendTouchCoordinatesDone(error: NSError?, data: NSData?, statusCode: Int) {
        let text = NSString(data: data!, encoding: NSUTF8StringEncoding)
        
        // if we get response from the server will check the response
        if ( (statusCode == 200) && (error == nil) && (data != nil) ) {
            // do nothing
        }
    }
    
    // MEETINGS
    func getMeetingsFromServer(controller: MeetingsViewController!) {
        meetingsDoneDelegate = controller
        Server.getMeetings(getMeetingsDone)
    }
    
    private func getMeetingsDone(error: NSError?, data: NSData?, statusCode: Int) {
        let text = NSString(data: data!, encoding: NSUTF8StringEncoding)
        var message: String = ""
        var success: Bool = false
        
        // if we get response from the server will check the response
        if ( (statusCode == 200) && (error == nil) && (data != nil) ) {
            let json = JSON(data: data!)
            
            message = json["message"].stringValue
            // if success = false we do not have any more a valid token
            if (json["success"].boolValue == true) {
                success = true
                if (json["data"]["meetings"].count > 0) {
                    if (meetings.count > 0) {
                        meetings.removeAll(keepCapacity: true)
                    }
                    
                    let jsonFiles = json["data"]["meetings"]
                    for (index: String, subJson: JSON) in jsonFiles {
                        meetings.append(Meeting(id: subJson["id"].stringValue, name: subJson["name"].stringValue, startDate: subJson["startDate"].stringValue, endDate: subJson["endDate"].stringValue, observation: subJson["obs"].stringValue))
                    }

                    MeetingDao(database: database).saveAll()
                    
                    if (currentMeeting != nil) {
                        var isValidCurrentMeeting = false
                        for meeting in Global.dataManager.meetings {
                            if currentMeeting.id == meeting.id {
                                isValidCurrentMeeting = true
                                break
                            }
                        }
                        
                        if (isValidCurrentMeeting == false) {
                            currentMeeting = nil
                        }
                    }
                    
                    if (currentMeeting == nil) {
                        if (meetings.count > 0) {
                            setCurrentMeeting(meetings[0])
                        } else {
                            setCurrentMeeting(nil)
                        }
                    }
                }
            }
        }
        
        if (meetingsDoneDelegate != nil) {
            meetingsDoneDelegate.GetMeetingsDone(success, statusCode: statusCode, message: message)
        }
    }
    
    func getMeetingsFromDatabase(controller: MeetingsViewController!, setCurrent: Bool = true) {
        meetingsDoneDelegate = controller
        MeetingDao(database: database).getAll()

        if (setCurrent == true) {
            if (currentMeeting != nil) {
                var isValidCurrentMeeting = false
                for meeting in Global.dataManager.meetings {
                    if currentMeeting.id == meeting.id {
                        isValidCurrentMeeting = true
                        break
                    }
                }
                
                if (isValidCurrentMeeting == false) {
                    currentMeeting = nil
                }
            }
            
            if (currentMeeting == nil) {
                if (meetings.count > 0) {
                    setCurrentMeeting(meetings[0])
                } else {
                    setCurrentMeeting(nil)
                }
            }
        }

        
        if (meetingsDoneDelegate != nil) {
            meetingsDoneDelegate.GetMeetingsDone(true, statusCode: 200, message: "")
        }
    }
    
    // SCREENS
    func getScreensFromServer(controller: ScreensViewController!) {
        screensDoneDelegate = controller
        Server.getScreens(getScreensDone)
    }
    
    private func getScreensDone(error: NSError?, data: NSData?, statusCode: Int) {
        var message: String = ""
        var success: Bool = false
        
        // if we get response from the server will check the response
        if ( (statusCode == 200) && (error == nil) && (data != nil) ) {
            let json = JSON(data: data!)
            
            message = json["message"].stringValue
            // if success = false we do not have any more a valid token
            if (json["success"].boolValue == true) {
                success = true
                if (json["data"]["screens"].count > 0) {
                    if screens.count > 0 {
                        screens.removeAll(keepCapacity: true)
                    }
                    
                    let jsonScreens = json["data"]["screens"]
                    for (index: String, subJson: JSON) in jsonScreens {
                        screens.append(Screen(id: subJson["id"].stringValue, name: subJson["name"].stringValue))
                    }
                    
                    ScreenDao(database: database).saveAll()
                                        
                    if (currentScreen != nil) {
                        var isValidCurrentScreen = false
                        for screen in Global.dataManager.screens {
                            if currentScreen.id == screen.id {
                                isValidCurrentScreen = true
                                break
                            }
                        }
                        
                        if (isValidCurrentScreen == false) {
                            currentScreen = nil
                        }
                    }
                    
                    if (currentScreen == nil) {
                        setCurrentScreen(nil)
                    }
                }
            }
        }
        
        if (screensDoneDelegate != nil) {
            screensDoneDelegate.GetScreensDone(success, statusCode: statusCode, message: message)
        }
    }
    
    func getScreensFromDatabase(controller: ScreensViewController!, setCurrent: Bool = true) {
        screensDoneDelegate = controller
        ScreenDao(database: database).getAll()
        
        if (setCurrent == true) {
            if (currentScreen != nil) {
                var isValidCurrentScreen = false
                for screen in Global.dataManager.screens {
                    if currentScreen.id == screen.id {
                        isValidCurrentScreen = true
                        break
                    }
                }
                
                if (isValidCurrentScreen == false) {
                    currentScreen = nil
                }
            }
            
            if (currentScreen == nil) {
                setCurrentScreen(nil)
            }
        }

        if (screensDoneDelegate != nil) {
            screensDoneDelegate.GetScreensDone(true, statusCode: 200, message: "")
        }
    }
    
    func activateScreen(controller: ScreensViewController, idMeeting: String, idScreen: String) {
        screensDoneDelegate = controller
        
        let idString = "{\"idMeeting\":\"\(idMeeting)\", \"idScreen\":\"\(idScreen)\"}"
        if let idData = (idString as NSString).dataUsingEncoding(NSUTF8StringEncoding) {
            Server.activateScreen(idData, done: activateScreenDone)
        }
    }
    
    private func activateScreenDone(error: NSError?, data: NSData?, statusCode: Int) {
        let text = NSString(data: data!, encoding: NSUTF8StringEncoding)
        var message: String = ""
        var success: Bool = false
        
        // if we get response from the server will check the response
        if ( (statusCode == 200) && (error == nil) && (data != nil) ) {
            let json = JSON(data: data!)
            
            message = json["message"].stringValue
            // if success = false we do not have any more a valid token
            if (json["success"].boolValue == true) {
                success = true
            }
        }
        
        if (screensDoneDelegate != nil) {
            screensDoneDelegate.ActivateScreenDone(success, statusCode: statusCode, message: message)
        }
    }
    
    // FILES
    func getFilesFromServer(controller: FilesViewController!) {
        filesDoneDelegate = controller
        Server.getFiles(getFilesDone)
    }
    
    private func getFilesDone(error: NSError?, data: NSData?, statusCode: Int) {
        let text = NSString(data: data!, encoding: NSUTF8StringEncoding)
        var message: String = ""
        var success: Bool = false
        
        // if we get response from the server will check the response
        if ( (statusCode == 200) && (error == nil) && (data != nil) ) {
            let json = JSON(data: data!)
            
            message = json["message"].stringValue
            // if success = false we do not have any more a valid token
            if (json["success"].boolValue == true) {
                success = true
                if (json["data"]["files"].count > 0) {
                    if (files.count > 0) {
                        files.removeAll(keepCapacity: true)
                    }
                    
                    let jsonFiles = json["data"]["files"]
                    for (index: String, subJson: JSON) in jsonFiles {
                        let isPrivate = subJson["isPrivate"].boolValue
                        files.append(File(id: subJson["id"].stringValue, name: subJson["name"].stringValue, type: subJson["type"].stringValue, isPrivate: subJson["isPrivate"].boolValue))
                    }

                    FileDao(database: database).saveAll()
                    
                    if (currentFile != nil) {
                        var isValidCurrentFile = false
                        for file in Global.dataManager.files {
                            if currentFile.id == file.id {
                                isValidCurrentFile = true
                                break
                            }
                        }
                        
                        if (isValidCurrentFile == false) {
                            currentFile = nil
                        }
                    }
                    
                    if (currentFile == nil) {
                        setCurrentFile(nil)
                    }
                }
            }
        }
        
        if (filesDoneDelegate != nil) {
            filesDoneDelegate.GetFilesDone(success, statusCode: statusCode, message: message)
        }
    }
    
    func getFilesFromDatabase(controller: FilesViewController!, setCurrent: Bool = true) {
        filesDoneDelegate = controller
        FileDao(database: database).getAll()
        
        if (setCurrent == true) {
            if (currentFile != nil) {
                var isValidCurrentFile = false
                for file in Global.dataManager.files {
                    if currentFile.id == file.id {
                        isValidCurrentFile = true
                        break
                    }
                }
                
                if (isValidCurrentFile == false) {
                    currentFile = nil
                }
            }
            
            if (currentFile == nil) {
                setCurrentFile(nil)
            }
        }
        
        if (filesDoneDelegate != nil) {
            filesDoneDelegate.GetFilesDone(true, statusCode: 200, message: "")
        }
    }
    
    func activateFile(controller: FilesViewController, idMeeting: String, idFile: String) {
        filesDoneDelegate = controller
        
        let idString = "{\"idMeeting\":\"\(idMeeting)\", \"idFile\":\"\(idFile)\"}"
        if let idData = (idString as NSString).dataUsingEncoding(NSUTF8StringEncoding) {
            Server.activateFile(idData, done: activateFileDone)
        }
    }
    
    private func activateFileDone(error: NSError?, data: NSData?, statusCode: Int) {
        let text = NSString(data: data!, encoding: NSUTF8StringEncoding)
        var message: String = ""
        var success: Bool = false
        
        // if we get response from the server will check the response
        if ( (statusCode == 200) && (error == nil) && (data != nil) ) {
            let json = JSON(data: data!)
            
            message = json["message"].stringValue
            // if success = false we do not have any more a valid token
            if (json["success"].boolValue == true) {
                success = true
            }
        }
        
        if (filesDoneDelegate != nil) {
            filesDoneDelegate.ActivateFileDone(success, statusCode: statusCode, message: message)
        }
    }
    
    // ANNOTATIONS
    func getAnnotationsFromServer() {
        Server.getAnnotations(getAnnotationsDone)
    }
    
    private func getAnnotationsDone(error: NSError?, data: NSData?, statusCode: Int) {
        let text = NSString(data: data!, encoding: NSUTF8StringEncoding)
        let y = data
        let z = error
    }
    
    func getAnnotationsFromDatabase() {
        
    }
    
    func newAnnotation(newXCoordinate: Float, newYCoordinate: Float, newText: String) {
        var newAnnotation = Annotation(xCoordinate: newXCoordinate, yCoordinate: newYCoordinate, text: newText, ID: 0)
        
        //upload to the server and get unique ID from server
        
        
        //save to database
        
    }
}