//
//  ScreenDao.swift
//  Meeting
//
//  Created by Lucian RADUTI on 13/01/15.
//  Copyright (c) 2015 QUALTEH. All rights reserved.
//

import Foundation

class ScreenDao {
    let table: ScreensTable =  ScreensTable()
    var database: COpaquePointer = nil

    init(database: COpaquePointer) {
        self.database = database
    }

    func saveAll() -> Bool{
        delete()
    
        for screen in Global.dataManager.screens {
            sqlite3_exec(database,
                "INSERT INTO \(table.table) (\(table.id), \(table.name)) VALUES (\"\(screen.id)\", \"\(screen.name)\")",
                nil, nil, nil)
        }
    
        return true
    }

    func delete() -> Bool {
        let query = "DELETE FROM \(table.table);"
        var errMsg:UnsafeMutablePointer<Int8> = nil
    
        if (sqlite3_exec(database, query, nil, nil, &errMsg) == SQLITE_OK) {
            return true;
        }
    
        return false;
    }

    func getAll() -> Bool {
        let query = "SELECT \(table.id), \(table.name) FROM \(table.table)"
        var statement:COpaquePointer = nil
    
        if sqlite3_prepare_v2(database, query, -1, &statement, nil) == SQLITE_OK {
            Global.dataManager.screens.removeAll(keepCapacity: true)
            
            while sqlite3_step(statement) == SQLITE_ROW {                
                Global.dataManager.screens.append(
                    Screen(
                        id: String.fromCString(UnsafePointer<CChar>(sqlite3_column_text(statement, 0))),
                        name: String.fromCString(UnsafePointer<CChar>(sqlite3_column_text(statement, 1)))
                    )
                )
            }
            sqlite3_finalize(database)
            
            return true
        }
    
        return false
    }
}