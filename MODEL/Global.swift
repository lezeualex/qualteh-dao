//
//  GlobalVariables.swift
//  Meeting
//
//  Created by Lucian RADUTI on 15/01/15.
//  Copyright (c) 2015 QUALTEH. All rights reserved.
//

import Foundation

enum TabBarIndex:Int {
    case Touchpad = 0
    case Annotations = 1
    case Screens = 2
    case Files = 3
    case Meetings = 4
    case Settings = 5
    case Login = 6
    case Help = 7
    static let all = [Touchpad, Annotations, Screens, Files, Meetings, Settings, Login, Help]
}

struct Global {
    static let databaseName = "dataIQboardMeeting.sqlite"
    static var dataManager: DataManager = DataManager()
    
    // LOAD DATA: SERVER URL, USERNAME AND TOKEN, MEETINGS, SCREENS, FILES
    static func initData() {
        dataManager.initData()
    }
    
    static func deinitData() {
        dataManager.deinitData()
    }
    
    // SETTINGS
    static func saveSettings() {
        dataManager.saveSettingsToDatabase()
    }
    
    // TEST SERVER CONNECTION
    static func testServerConnection(controller: SettingsViewController)
    {
        dataManager.testServerConnection(controller)
    }
    
    // USER
    static func loginUser(controller: LoginViewController, userName: String, userPassword: String) {
        dataManager.loginUser(controller, userName: userName, userPassword: userPassword)
    }
    
    static func getToken() -> String {
        return dataManager.user.token
    }
    
    static func getUser() -> User {
        return dataManager.user
    }
    
    static func checkUserToken(controller: TouchpadViewController) {
        dataManager.checkUserToken(controller)
    }
    
    static func isUserValid() -> Bool {
        return dataManager.isUserValid()
    }
    
    // MEETINGS
    static func getMeetingsFromServer(controller: MeetingsViewController!) {
        dataManager.getMeetingsFromServer(controller)
    }
    
    static func getMeetingsFromDatabase(controller: MeetingsViewController!) {
        dataManager.getMeetingsFromDatabase(controller)
    }
    
    // TOUCH
    static func sendTouchCoordinates(xCoordinate: Float, yCoordinate: Float)
    {
        dataManager.sendTouchCoordinates(xCoordinate, yCoordinate: yCoordinate)
    }
    
    // SCREENS
    static func getScreensFromServer(controller: ScreensViewController!) {
        dataManager.getScreensFromServer(controller)
    }
    
    static func getScreensFromDatabase(controller: ScreensViewController!) {
        dataManager.getScreensFromDatabase(controller)
    }
    
    static func activateScreen(controller: ScreensViewController, idMeeting: String, idScreen: String) {
        dataManager.activateScreen(controller, idMeeting: idMeeting, idScreen: idScreen)
    }
    
    // FILES
    static func getFilesFromServer(controller: FilesViewController!) {
        dataManager.getFilesFromServer(controller)
    }
    
    static func getFilesFromDatabase(controller: FilesViewController!) {
        dataManager.getFilesFromDatabase(controller)
    }
    
    static func activateFile(controller: FilesViewController, idMeeting: String, idFile: String) {
        dataManager.activateFile(controller, idMeeting: idMeeting, idFile: idFile)
    }
}