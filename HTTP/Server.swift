//
//  ServerRequest.swift
//  Meeting
//
//  Created by Lucian RADUTI on 12/01/15.
//  Copyright (c) 2015 QUALTEH. All rights reserved.
//

import Foundation

class Server: NSObject {
    var request: NSMutableURLRequest? = nil
    var response: NSHTTPURLResponse? = nil
    var statusCode: Int = 0
    
    var data: NSMutableData? = nil
    var done: (NSError?, NSData?, Int) -> () = { (_, _, _) -> () in }
    
    init (method: String, url: String, parameter: NSData?, done: (NSError?, NSData?, Int) -> ()) {
        let url = NSURL(string: url)
        self.request = NSMutableURLRequest(URL: url!)
        
        
        if ((parameter) != nil) {
            self.request!.HTTPBody = parameter!
        }
        
        self.request!.HTTPMethod = method
        self.request!.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        self.done = done
        self.data = NSMutableData()
    }
    
    func start () {
        let connection = NSURLConnection(request: self.request!, delegate: self, startImmediately: true)
    }
    
    class func testConnection(done: (NSError?, NSData?, Int) -> ()) -> Void {
        return Server(method: "GET", url: Global.dataManager.settings.serverURL + "/testconnection", parameter: nil, done: done).start()
    }
    
    class func login(parameter: NSData, done: (NSError?, NSData?, Int) -> ()) -> Void {
        return Server(method: "POST", url: Global.dataManager.settings.serverURL + "/login", parameter: parameter, done: done).start()
    }
    
    class func checkUserToken(done: (NSError?, NSData?, Int) -> ()) -> Void {
        return Server(method: "GET", url: Global.dataManager.settings.serverURL + "/verifytoken/" + Global.getToken(), parameter: nil, done: done).start()
    }
    
    class func getMeetings(done: (NSError?, NSData?, Int) -> ()) -> Void {
        return Server(method: "GET", url: Global.dataManager.settings.serverURL + "/mobilemeetings/" + Global.getToken(), parameter: nil, done: done).start()
    }
    
    class func getScreens(done: (NSError?, NSData?, Int) -> ()) -> Void {
        return Server(method: "GET", url: Global.dataManager.settings.serverURL + "/mobilescreens/" + Global.getToken(), parameter: nil, done: done).start()
    }
    
    class func activateScreen(parameter: NSData, done: (NSError?, NSData?, Int) -> ()) -> Void {
        return Server(method: "POST", url: Global.dataManager.settings.serverURL + "/displayscreenondevice/" + Global.getToken(), parameter: parameter, done: done).start()
    }
    
    class func getFiles(done: (NSError?, NSData?, Int) -> ()) -> Void {
        return Server(method: "GET", url: Global.dataManager.settings.serverURL + "/mobilefiles/" + Global.getToken(), parameter: nil, done: done).start()
    }
    
    class func activateFile(parameter: NSData, done: (NSError?, NSData?, Int) -> ()) -> Void {
        return Server(method: "POST", url: Global.dataManager.settings.serverURL + "/makescreenfromfile/" + Global.getToken(), parameter: parameter, done: done).start()
    }
    
    class func getAnnotations(done: (NSError?, NSData?, Int) -> ()) -> Void {
        return Server(method: "GET", url: Global.dataManager.settings.serverURL + "/annotationsscreens/" + Global.getToken(), parameter: nil, done: done).start()
    }
    
    class func sendTouchCoordinates(parameter: NSData, done: (NSError?, NSData?, Int) -> ()) -> Void {
        return Server(method: "POST", url: Global.dataManager.settings.serverURL + "/touchcoordinate/" + Global.getToken(), parameter: parameter, done: done).start()
    }

    func connection(connection: NSURLConnection!, didReceiveData data: NSData!) {
        self.data!.appendData(data)
    }
    
    func connection(connection: NSURLConnection!, didReceiveResponse response: NSHTTPURLResponse!) {
        self.response = response
        statusCode = response.statusCode
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!) {
        self.done(nil, self.data!, statusCode)
    }
    
    func connection(connection: NSURLConnection!, didFailWithError error: NSError!) {
        println(error.description)
        self.done(error, nil, statusCode)
    }
    
    func getStatusCode() -> Int {
        if let statusCode = self.response?.statusCode {
            return statusCode
        }
        
        return 0
    }
    
    func isStatusOK() -> Bool {
        if let statusCode = self.response?.statusCode {
            if statusCode == 200 {
                return true
            }
        }
        
        return false
    }
}